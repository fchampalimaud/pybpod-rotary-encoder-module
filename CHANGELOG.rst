Changelog
=========

0.1.3 (2019-07-23)
------------------
* Minor documentation fixes
* Fixed ReadTheDocs requirements

0.1.2 (2019-07-19)
------------------
* Added ComboBox and a refresh button for the serial port selection for the Rotary Encoder
